# first-test-cypress

I have created a small login test project in which the Angular framework is used and 
for testing I have used Cypress.

# What is Cypress?

Cypress is a next generation front end testing tool built for the modern web. We address the key pain points developers and QA engineers face when testing modern applications.

We make it possible to:

* Set up tests
* Write tests
* Run tests
* Debug Tests

Cypress is most often compared to Selenium; however Cypress is both fundamentally and architecturally different. 
Cypress is not constrained by the same restrictions as Selenium.

This enables you to write faster, easier and more reliable tests.

# Who uses Cypress?

Our users are typically developers or QA engineers building web applications using modern JavaScript frameworks.

Cypress enables you to write all types of tests:

* End-to-end tests
* Integration tests
* Unit tests
* Cypress can test anything that runs in a browser.

# Prerequisites

Cypress is a desktop application that is installed on your computer. The desktop application supports these operating systems:

* macOS 10.9 and above (64-bit only)
* Linux Ubuntu 12.04 and above, Fedora 21 and Debian 8 (64-bit only)
* Windows 7 and above

If using npm to install Cypress, we support:


* Node.js 8 and above 

#Installing

install Cypress via npm:

cd /your/project/path
npm install cypress --save-dev

# Direct Download

If you’re not using Node or npm in your project or you want to try Cypress out quickly,
you can always download Cypress directly from the CDN.

# Running The Test

Now you can open Cypress from your project root one of the following ways:

The long way with the full path

**./node_modules/.bin/cypress open
Or with the shortcut using npm bin**

**$(npm bin)/cypress open
Or by using npx**

**npx cypress open**

When you run the cypress it will create a Cypress folder and in that folder there will be an
Integration sub-folder where we have to create a test file where we can write
our tests.

I have created a small login test project in which I have integrated Cypress.
In this test cypress will authenticate the login. 
1. It will visit the web application.
2. Then it will check if the credentials are empty.
3. If its empty then it will ask to enter the credentials.
4. On next step cypress will enter the incorrect credentials.
5. Upon entering the credintials it will click on the login button and auntenticate.

# Built With

1. Angular 8
2. Cypress

# Acknowledgments

1. Cypress.io
2. demo.nocommerce.com
